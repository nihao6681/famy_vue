import Vue from 'vue'
import Router from 'vue-router'
import christmasEves from "../view/christmasEve"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/christmasEves',
      component: christmasEves
    }
  ]
})
